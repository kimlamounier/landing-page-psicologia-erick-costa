import React from "react";
import JsonData from "../data/data.json";

export const FloatingButton = () => {
  return (
    <div id="fab-button">
        <a href={JsonData.Links.whatsapp} class="btn btn-primary btn-lg floating-button">
            <i class="fa fa-whatsapp"></i>
        </a>
    </div>
  );
};
