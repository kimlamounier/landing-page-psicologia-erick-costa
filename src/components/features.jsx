import React from "react";
import BrainIcon from '../icons/brain.svg'
import CloudSadIcon from '../icons/cloud-sad.svg'
import TalkIcon from '../icons/talk.svg'
import LightIcon from '../icons/light.svg'

export const Features = (props) => {
  function getImage(value) {
      if (value === 'brain') {
        return BrainIcon
      }
      else if(value === 'cloud-sad'){
        return CloudSadIcon
      }
      else if(value === 'talk'){
        return TalkIcon
      }
      else if(value === 'light'){
        return LightIcon
      }
  }
  return (
    <div id="features">
      <div className="container overlay">
        <div className="row margin-bottom">
          <div className="col-md-12 section-title">
            <div className="feature-text">
              <h2>{props.data ? props.data.title : ""}</h2>
              <p>{props.data ? props.data.text : ""}</p>
              <p>{props.data ? props.data.text1 : ""}</p>
              <h3>{props.data ? props.data.subtitle : ""}</h3>
              <div className="list-style">
                <div className="col-lg-6 col-sm-6 col-xs-12 fullwidth">
                  <ul>
                    {props.data
                      ? props.data.learn.map((d, i) => (
                          <li key={`${d}-${i}`}>{d}</li>
                        ))
                      : ""}
                  </ul>
                </div>
              </div>
              <p>{props.data ? props.data.text2 : ""}</p>
              <p>{props.data ? props.data.text3 : ""}</p>
            </div>
          </div>
          <div className="col-md-12 section-title section-title-continue">
            <div className="feature-text">
              <h2>{props.data ? props.data.titleTcc : ""}</h2>
              <p>{props.data ? props.data.textTcc : ""}</p>
              <h3>{props.data ? props.data.subtitleTcc : ""}</h3>
              <div className="list-style no-padding">
                  <ul className="ul-features">
                    {props.data
                      ? props.data.tecnicasTcc.map((d, i) => (
                        <div key={`${d.title}-${i}`} className="alignment no-padding">
                        <div className="div-circle">
                          <img src={getImage(d.icon)} alt="" />
                        </div>
                        <h4 className="techniques title">{d.title}</h4>
                        <p className="techniques description">{d.text}</p>
                      </div>
                        ))
                      : ""}
                  </ul>
              </div>
              <h3>{props.data ? props.data.subtitle1Tcc : ""}</h3>
              <div className="list-style margin div-center">
                  <ul className="ul-grid">
                    {props.data
                      ? props.data.psicTcc.map((d, i) => (
                          <li className="list-style-grid" key={`${d}-${i}`}>{d}</li>
                        ))
                      : ""}
                  </ul>
              </div>
              <p>{props.data ? props.data.text2Tcc : ""}</p>
            </div>
          </div>
          {/* <div className="col-md-12 section-title section-title-continue">
            <div className="feature-text">
              <h2>{props.data ? props.data.titleChoice : ""}</h2>
              <p>{props.data ? props.data.textChoice : ""}</p>
            </div>
          </div> */}
        </div>
      </div>
    </div>
  );
};