import React from "react"
import MeditationIcon from '../icons/meditation.svg'
import CureIcon from '../icons/cure.svg'
import HealthyIcon from '../icons/healthy.svg'
import GroupIcon from '../icons/group.svg'
import MedicalIcon from '../icons/house-medical.svg'
import AddictionIcon from '../icons/addiction.svg'

export const Services = (props) => {
  function getImage(value) {
    if (value === 'meditation') {
      return MeditationIcon
    }
    else if(value === 'cure'){
      return CureIcon
    }
    else if(value === 'healthy'){
      return HealthyIcon
    }
    else if(value === 'group'){
      return GroupIcon
    }
    else if(value === 'medical'){
      return MedicalIcon
    }
    else if(value === 'addiction'){
      return AddictionIcon
    }
}
  return (
    <div id="services" className="text-center">
      <div className="container service-container">
        <div className="section-title">
          <h2>{props.data ? props.data.title : ""}</h2>
          <p>
            {props.data ? props.data.description : ""}
          </p>
        </div>
        <div className="list-style">
          <div className="col-lg-6 col-sm-6 col-xs-12 fullwidth">
            <ul className="ul-services">
              {props.data
                ? props.data.items.map((d, i) => (
                  <div key={`${d.title}-${i}`} className="alignment">
                  <div className="div-circle">
                    <img src={getImage(d.icon)} alt="" />
                  </div>
                  <h4>{d.name}</h4>
                  <p className="techniques description-benefits">{d.text}</p>
                </div>
                  ))
                : ""}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
