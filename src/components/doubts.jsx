import React from 'react';
import DownArrowIcon from '../icons/arrow-down.svg'

export const Doubts = (props) => {
  return (
    <div id="doubts">
      <div className="container">
        <div className="section-title">
          <h2>Dúvidas Frequentes</h2>
          <div id="accordion">
            {props.data
              ? props.data.map((d, i) => (
                <div class="panel card div-card">
                <div class="card-header" id={`heading-${i}`}>
                  <h5 class="mb-0">
                    <button class={`btn btn-link btn-accordion collapse collapsed`} data-toggle="collapse" data-target={`#collapse-${i}`} aria-controls={`collapse-${i}`}>
                      {d.title}
                      <img id="icon" data-target={`#collapse-${i}`} src={DownArrowIcon} alt="" />
                    </button>
                  </h5>
                </div>
                <div id={`collapse-${i}`} class="collapse" aria-labelledby={`heading-${i}`} data-parent="#accordion">
                  <div class="card-body">
                    {d.text}
                  </div>
                </div>
              </div>
              )): ""}
          </div>
        </div>
      </div>
    </div>
  );
};
