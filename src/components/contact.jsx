import React from "react";

export const Contact = (props) => {
  return (
    <div>
      <div id="contact">
        <div className="container">
          <div className="col-md-12">
            <div className="row">
              <div className="section-title">
                <h2>Contato</h2>
                <div className="contact-display">
                  <div className="contact-item no-margin">
                      <div>
                        <span>
                          <img src="img/white-logo.png" className="img-responsive-squared" alt="" />
                        </span>
                        <div>
                          <span>
                            {props.data ? props.data.profession : ""}
                          </span>
                          <span>
                            {props.data ? props.data.approach : ""}
                          </span>
                          <span>
                            {props.data ? props.data.service : ""}
                          </span>
                        </div>
                      </div>
                    </div>
                  <div className="content">
                    <div className="contact-item">
                      <p>
                        <span>
                          <i className="fa fa-whatsapp"></i> Whatsapp
                        </span>
                        {props.data ? props.data.phone : ""}
                      </p>
                    </div>
                    <div className="contact-item">
                      <span>
                          <i className="fa fa-instagram"></i> Instagram
                      </span>
                      <p>
                        {props.data ? props.data.instagram : ""}
                      </p>
                    </div>
                    <div className="contact-item">
                      <p>
                        <span>
                          <i className="fa fa-envelope-o"></i> Email
                        </span>
                        {props.data ? props.data.email : ""}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
