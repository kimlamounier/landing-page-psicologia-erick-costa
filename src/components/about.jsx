import React from "react";
import JsonData from "../data/data.json";

export const About = (props) => {
  return (
    <div id="about">
      <div className="container">
        <div className="row grid">
          <div className="div-image">
            <img src="img/foto-2.png" className="img-responsive" alt="" />
          </div>
          <div className="panel-body">
              <h2>{props.data ? props.data.title : ""}</h2>
              <p>{props.data ? props.data.paragraph : ""}</p>
              <p>{props.data ? props.data.paragraph2 : ""}</p>
              <p>{props.data ? props.data.paragraph3 : ""}</p>
              <div className="btn-about">
                <a
                    href={JsonData.Links.whatsapp}
                    className="btn btn-custom btn-lg page-scroll"
                  >
                    {JsonData.Links.textButton}
                </a>
              </div>
          </div>
        </div>
      </div>
    </div>
  )
};
