import React from "react";
import JsonData from "../data/data.json";

export const Header = (props) => {
  return (
    <header id="header">
      <div className="intro">
        <div className="overlay">
          <div className="container">
            <div className="introduction row">
              <div className="col-md-8 col-md-offset-2 intro-text">
                <p>{props.data ? props.data.dataPsi : ""}</p>
                <h2 className="h2-title">
                  {props.data ? props.data.title : ""}
                  <span></span>
                </h2>
                <ul>
                  <li className="list-intro">{props.data ? props.data.service : ""}</li>
                  <li className="list-intro">{props.data ? props.data.approach : ""}</li>
                  <li className="list-intro">{props.data ? props.data.development : ""}</li>
                  <li className="list-intro">{props.data ? props.data.followUp : ""}</li>
                </ul>
                <a
                  href={JsonData.Links.whatsapp}
                  className="btn btn-custom btn-lg page-scroll btn-intro"
                >
                  {JsonData.Links.textButton}
                </a>
              </div>
              <img src="img/foto-1.png" className="introduction-image" alt="Foto do psicólogo" />
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};
