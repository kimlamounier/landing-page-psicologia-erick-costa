import React from "react";

export const Navigation = (props) => {

  const detectMob = () => {
    const toMatch = [
      /Android/i,
      /webOS/i,
      /iPhone/i,
      /BlackBerry/i,
      /Windows Phone/i
    ]
  
    return toMatch.some((toMatchItem) => {
        return navigator.userAgent.match(toMatchItem)
  })}

  const mobile = () => (
    <div
          className="collapse navbar-collapse"
          id="bs-example-navbar-collapse-1"
        >
          <ul className="nav navbar-nav navbar-right">
            <li>
              <a href="#page-top" className="page-scroll" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                Início
              </a>
            </li>
            <li>
              <a href="#about" className="page-scroll" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                Sobre mim
              </a>
            </li>
            <li>
              <a href="#features" className="page-scroll" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                Psicoterapia
              </a>
            </li>
            <li>
              <a href="#services" className="page-scroll" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                Benefícios
              </a>
            </li>
            <li>
              <a href="#doubts" className="page-scroll" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                Dúvidas Frequentes
              </a>
            </li>
            <li>
              <a href="#contact" className="page-scroll" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                Contato
              </a>
            </li>
          </ul>
        </div>
  )

  const notMobile = () => (
    <div
          className="collapse navbar-collapse"
          id="bs-example-navbar-collapse-1"
        >
          <ul className="nav navbar-nav navbar-right">
            <li>
              <a href="#page-top" className="page-scroll">
                Início
              </a>
            </li>
            <li>
              <a href="#about" className="page-scroll">
                Sobre mim
              </a>
            </li>
            <li>
              <a href="#features" className="page-scroll">
                Psicoterapia
              </a>
            </li>
            <li>
              <a href="#services" className="page-scroll">
                Benefícios
              </a>
            </li>
            <li>
              <a href="#doubts" className="page-scroll">
                Dúvidas Frequentes
              </a>
            </li>
            <li>
              <a href="#contact" className="page-scroll">
                Contato
              </a>
            </li>
          </ul>
        </div>
  )

  return (
    <nav id="menu" className="navbar navbar-default navbar-fixed-top">
      <div className="container container-web">
        <div className="navbar-header navbar-header-mobile">
          <a className="navbar-brand page-scroll navbar-mobile" href="#page-top">
            <img src="img/logo-horizontal.png" className="img-fit" alt="" />
          </a>
          <button
            type="button"
            className="navbar-toggle collapsed"
            data-toggle="collapse"
            data-target="#bs-example-navbar-collapse-1"
          >
            <span className="sr-only">Menu</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>

        </div>
      {detectMob() === true ? mobile() : notMobile()}
      </div>
    </nav>
  );
};
